"""Descargue la fuente de datos relacionada con vacunación contra el sarampión
 en niños entre 12-23 meses en Panamá del Word Bank y diseñe un API tipo REST
 de sólo-lectura que manipule estos datos."""

import json
from flask import Flask
from flask_restful import Api, Resource, reqparse

def cargar_datos(ruta):
    with open(ruta) as contenido:
        data = json.load(contenido)
        return data

ruta = "data.json"

app = Flask(__name__)
api = Api(app)


class PANImmunization(Resource):
    def get(self, id=0):
        if id == 0:
           cargar_datos(ruta)
           return cargar_datos(ruta), 200

        if id < len(cargar_datos(ruta)) and id >= 0:
           return cargar_datos(ruta)[id], 200
        return "No hay resultado", 404

@app.route('/')
def index():
    return '<!DOCTYPE html><head><style> @import url("https://fonts.googleapis.com/css2?family=Krona+One&display=swap");body{font-family: "Krona One", sans-serif;} </style></head><body style="padding:100px;overflow-x:hidden;"><div style="display:flex;width:100vw;"><img src="https://www.pngkit.com/png/full/49-496133_the-world-bank-logo-world-bank-logo-transparent.png" width="90%" heith="auto"/></div><h1>Programación IV</h1><div><h2>Rutas:</h2><ul><li>Ver todos los registros:<a href="http://127.0.0.1:5000/vacunacion-panama"> http://127.0.0.1:5000/vacunacion-panama</a></li><li>Ver especifico registro: <a href="http://127.0.0.1:5000/vacunacion-panama/1">http://127.0.0.1:5000/vacunacion-panama/:id</a></ul></div></body></html>',200
api.add_resource(PANImmunization, "/vacunacion-panama", "/vacunacion-panama/", "/vacunacion-panama/<int:id>")

if __name__ == '__main__':
    app.run(debug=True)
    #print(cargar_datos(ruta))
